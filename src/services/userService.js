import axios from 'axios';
import authHeader from './authHeader';

const API_URL = 'http://localhost:8085/';

class UserService {
  getPublicContent() {
    return axios.get(API_URL);
  }

  getUserView() {
    return axios.get(API_URL + 'user', { headers: authHeader() });
  }

}

export default new UserService();