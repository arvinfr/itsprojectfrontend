import axios from 'axios';

const API_URL = 'http://localhost:8085/'; 

class AuthService {
  login(user) {
    return axios
      .post(API_URL + 'authenticate', {
        userName: user.username,
        password: user.password
      })
      .then(response => {
        let return_obj = {}
        if (response.data.jwt) {
          axios.get(API_URL + 'login/' + user.username, {
            headers: {
              'Authorization' : `Bearer ${response.data.jwt}`
            },
          })
          .then(response => {
          return_obj = response.data;
        })}

        return_obj['jwt'] = response.data['jwt'];
        localStorage.setItem('user', JSON.stringify(return_obj));
        return return_obj;
      });
  }

  logout() {
    localStorage.removeItem('user');
  }

}

export default new AuthService();